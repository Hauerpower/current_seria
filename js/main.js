const menu = document.querySelector('.navTrigger')
const mobileNav = document.querySelector('.mobile-nav')
const subMenuLink = document.querySelector('.has-submenu');
const subMenu = document.querySelector('.submenu')
const mobileSubMenuLink = document.querySelector('.mobile-have-submenu')
const mobileNavActive = document.querySelector('.mobile-nav active')
const logo = document.querySelector('.logo-block')
const subMenuArrow = document.querySelector('.mobile-have-submenu .sub-submenu-arrow')


menu.addEventListener('click', ()=>{
  if (mobileNav.style.display === "none") {
    mobileNav.style.display = "block";
    setTimeout(()=>{
      mobileNav.classList.toggle('active')
    },150)
  } else {
    mobileNav.classList.toggle('active')
    setTimeout(()=>{
      mobileNav.style.display = "none";
    },500)
  }
  menu.classList.toggle('active')
  logo.classList.toggle('hidden')
  if(menu.classList.contains('active')){
    document.querySelector('.show').style.display = 'none'
  }else{
    document.querySelector('.show').style.display = 'block'
  }
})

mobileSubMenuLink.addEventListener('click', function(){
  mobileSubMenuLink.classList.toggle('active')
  subMenuArrow.classList.toggle('active')
  if(this.classList.contains('active')){
    mobileNav.style.height='91vh';
  }else{
    mobileNav.style.height="100vh"
  }
})

$('.search-header').click(function(e){
  e.preventDefault()
  $('.search-popup').toggleClass('active')
  $('.form-wrapper').toggleClass('active')
  $('.search-input').focus()
  if($('.search-popup').hasClass('active')){
    $('body').css({
      overflow: 'hidden',
    })
  }
})

$('.search-popup').click(function(e){
  e.preventDefault()
  
  if (e.target.classList.contains('close-search') || e.target.classList.contains('search-popup')){
    $('.search-popup').toggleClass('active')
    $('.form-wrapper').toggleClass('active')
    $('.search-form')[0].reset();
    $('.shadow-form')[0].reset()
    $('body').css({
      overflow: 'unset',
    })
  }  
})

$( ".search-input" )
  .keyup(function() {
    $('.form-text').val($(this).val())
  })




const panelsNavigation = document.querySelector('.panels-and-modules-navigation')
const linkWithSubMenu = document.querySelectorAll('.navigation-link-has-sub-menu')
const subMenuContainer = document.querySelector('.panels-and-modules-sub-menu')
const subMenuContainerLinks = subMenuContainer.getElementsByTagName('a')


panelsNavigation.addEventListener('click', linkAddCalss)

function linkAddCalss(e){
    linkWithSubMenu.forEach(item => {
    item.classList.remove('active')
  })
  if (e.target.classList.contains('sub-menu-link') ){  
    e.target.closest('li').classList.toggle('active')
  }
  if(e.target.classList.contains('more-link')){
    e.target.closest('li').classList.toggle('active')
  }
}

$('.mobile-serias-navigation').click(function(){
  $(this).toggleClass('active')
})


const showBtn = document.querySelector('.show')
// MENU SCROLL
showBtn.addEventListener('click', ()=>{
  panelsNavigation.classList.toggle('active')
  panelsNavigation.style.top = window.pageYOffset + 58 + 'px'
  showBtn.style.top = '-100px'
})

$(document).ready(function() {
  $(window).scroll(function() {
    panelsNavigation.classList.remove('active')
    panelsNavigation.style.top = '58px'
    if(this.window.pageYOffset === 0){

      showBtn.style.top = '-100px'
      if($(window).width() <= 640){
      }
      else if($(window).width() <= 991){
      }
      else if($(window).width() > 992){
      }

    }else{
      if(this.window.pageYOffset < 60){
        showBtn.style.top='-100px'
      }
      else if(this.window.pageYOffset > 80){
        showBtn.style.top='13px'
      }
        
      if($(window).width() <= 640){
      }
      else if($(window).width() <= 992){
      }
      else if($(window).width() > 992){
      }

    }
  });
});



  $('.panel').click(function(e){
    e.preventDefault()
    if($(e.target).hasClass('more-btn')){
      $(this).find('.panels-and-modules-description-table-parameters').toggleClass('parameters-open')
    }
  } 
)


